# Afvoeranalyse dashboard
Dit dashboard doet afvoeranalyses (en ook waterhoogteanalyses) op de reeksen uit ons WIS-systeem. Redelijk simpel aan te passen door aan te sluiten op eigen waterschaps WIS-systeem.
Dash is gebruikt om het dashboard te bouwen. Hieronder een indruk van het dashboard. Om een idee te krijgen van zo'n dashboard in actie, zie ook hier [dashboard wsbd](https://pilotverblijftijdenwatertemperatuur.brabantsedelta.nl/).

![dashboard](dashboard.png "Afvoeranalyse Dashboard")

## Analyse methodes
Er kunnen verschillende methodes worden gebruikt, een duurkromme met eventueel een factor uit het Cultuurtechnisch Vademecum (voor de extremen), de vaker voorkomende afvoeren (<T1) worden bepaald op basis van de duurkromme. Ook kan de generalized extreme value distribution worden gebruikt om een fit op de jaarmaxima te maken (werkt pas echt lekker als je +10 jaar hebt). Daarnaast kan je ook de mediane seizoensafvoeren bepalen.

## Gebruik
Een meetpunt kan uit de lijst worden geselecteerd, ook typ zoek functie, of op de kaart worden geselecteerd. De gehele meetreeks wordt dan binnen gehaald. De plots worden met de standaardmethode gemaakt. Periode van de analyse kan worden aangepast met de slider, en de methode kan worden aangepast met het dropdownmenu.

Dat was hem wel, mocht je er vragen over hebben kan je [mij](mailto:t.deurloo@brabantsedelta.nl) benaderen.
