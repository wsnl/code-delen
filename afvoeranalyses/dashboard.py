from dash.dependencies import Input, Output, State

import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
import pandas as pd

import dash
import dash_table
import func

#%% initialiseren van app
app = dash.Dash(__name__)
app.title = "Afvoeranalyse"

# Loading screen CSS
mapbox_access_token = 'pk.eyJ1IjoiYm95YW5kb21ob2YiLCJhIjoiY2pvNjd0cDRrMGU5NjNrbXh2MmV0bTdkcCJ9.LlTD3UJ3qkqMwMSTLE2N3w'

# eerst metadata via KiWIS opvragen van alle stations
df_meta = func.getMetaData('TDB')

# vervolgens de eerste in DataFrame gebruiken om data op te vragen
df = func.getData(df_meta['station_no'].iloc[0], df_meta['stationparameter_name'].iloc[0])
dff = df.copy()

# tot slot afvoeranalyse daarop loslaten
discharges, df_analysis = func.dischargeAnalysis(df, str(df_meta['name'].iloc[0]), str(df_meta['site_type_name'].iloc[0]), method='cultuur')
table = discharges['discharge'].transpose()

# sla gebruikte variabelen op voor vergelijking bij vernieuwing
store = {
    'station': str(df_meta['station_no'].iloc[0]),
    'period': [df.index.year.min(), df.index.year.max()],
    'analysis_method':  'cultuur'
}

#%% layout van de app
app.layout = html.Div([
                html.Div([
                        dcc.Tabs(
                        children=[dcc.Tab(label='Afvoeranalyse', value='TDB'),
        			  dcc.Tab(label='Waterhoogteanalyse', value='WTH')],
                value='TDB',
                id='tabs')], style={'display': 'inline-block', 'width': '30%'}),
                html.Div([
                    html.Div([
                        html.Div([
                                    html.Div(
                                        html.H4('STATION'), 
                                    style={'display': 'inline-block', 'margin': '0px 20px 0px 0px', 'width': '20%'}),
                                    html.Div(
                                        dcc.Dropdown(
                                            id='station',
                                            options=[{'label': label, 'value': value} for value, label in zip(df_meta['station_no'],df_meta['name'])],
                                            value=store['station']),
                                    style={'display': 'inline-block', 'width': '80%', 'vertical-align': 'middle'})
                                    ], style={'width': '100%'}
                        ),
                        html.Div([
                                    html.Div(
                                        html.H4('PERIODE'), 
                                    style={'display': 'inline-block', 'margin': '0px 20px 0px 0px', 'width': '20%'}),
                                    html.Div(
                                        dcc.RangeSlider(
                                            id='period',
                                            marks={int(round(i)): '{}'.format(int(round(i))) for i in np.linspace(df.index.year.min(), df.index.year.max(), num=10, endpoint=True)},
                                            min=df.index.year.min(),
                                            max=df.index.year.max(),
                                            step=1,
                                            value=store['period']),
                                    style={'display': 'inline-block', 'width': '80%', 'vertical-align': 'middle'})
                                    ], style={'width': '100%'}
                        ),
                        html.Div([
                                    html.Div(
                                        html.H4('METHODE'), 
                                    style={'display': 'inline-block', 'margin': '0px 20px 0px 0px', 'width': '20%'}),
                                    html.Div(
                                        dcc.Dropdown(
                                            id='analysis_method',
                                            options=[
                                                    {'label': 'Cultuurtechnisch Vademecum', 'value': 'cultuur'},
                                                    {'label': 'Gumbel', 'value': 'gumbel'},
                                                    {'label': 'Seizoens gemiddelde', 'value': 'seizoens'}
                                                    ],
                                            value=store['analysis_method']),
                                    style={'display': 'inline-block', 'width': '80%'})
                                    ], style={'width': '100%'}
                        )                                        
                    ], style={'display': 'inline-block', 'width': '50%', 'height': '300px', 'vertical-align': 'middle'}),
                    html.Div([
                            dcc.Graph(
                                    id='map',
                                    figure=dict(
                                        data=[go.Scattermapbox(
                                                lat=df_meta['station_latitude'], 
                                                lon=df_meta['station_longitude'], 
                                                mode='markers', 
                                                marker=dict(size=10, color='#e88709'), 
                                                hoverinfo='text',
                                                text=df_meta['name'],
                                            ),
                                            go.Scattermapbox(
                                                lat=[df_meta.loc[df_meta['station_no']==store['station'], 'station_latitude'].values[0]], 
                                                lon=[df_meta.loc[df_meta['station_no']==store['station'], 'station_longitude'].values[0]], 
                                                mode='markers',
                                                hoverinfo='text',
                                                marker=dict(size=1, color='#39e809'), 
                                                text=[df_meta.loc[df_meta['station_no']==store['station'], 'name'].values[0]],
                                        )], 
                                        layout=go.Layout(
                                                height=400,
                                                autosize=True, 
                                                hovermode='closest',
                                                mapbox=dict(
                                                    accesstoken=mapbox_access_token,
                                                    bearing=0,
                                                    center=dict(
                                                        lat=df_meta.loc[df_meta['station_no']==store['station'], 'station_latitude'].values[0],
                                                        lon=df_meta.loc[df_meta['station_no']==store['station'], 'station_longitude'].values[0]
                                                    ),
                                                    pitch=0,
                                                    zoom=10
                                                    ),
                                                margin=dict(r=0, t=0, b=0, l=0, pad=0),
                                                showlegend=False
                                                )
                                        )
                                    ) 
                    ], style={'display': 'inline-block', 'width': '50%', 'height': '300px', 'vertical-align': 'middle'})
                ]),
                html.Div(html.Div(id='graphs')),
            ], style={'width': '80%', 'margin': '0 auto'})

#%% callback voor updaten grafieken als iets wijzigt            
@app.callback(Output('graphs', 'children'),
              [Input('station', 'value'),
               Input('period', 'value'),
               Input('analysis_method', 'value'),
               Input('tabs', 'value')])

def update_graphs(station, period, analysis_method, tab):
    dummy = False
    
    global df, discharges, df_analysis, dff, store, df_meta
    
    # bij verandering van station, vraag nieuwe data op
    if not (station==store['station']):
        df = func.getData(station, df_meta['stationparameter_name'].iloc[0])
        dummy = True
        store['station'] = station
                   
    # bij verandering van periode, wijzig data DataFrame of verandering station
    if (not ((period[0]==store['period'][0]) and (period[1]==store['period'][1]))) or dummy:
        dff = df.loc[str(period[0]):str(period[1])]
        dummy = True
        store['period'] = [period[0], period[1]]

    # bij wijzigen methode afvoeranalyse, wijzig berekening of bij verandering station of periode
    if (not(str(analysis_method)==store['analysis_method'])) or dummy:
        discharges, df_analysis = func.dischargeAnalysis(dff, str(df_meta.loc[df_meta['station_no']==station, 'name'].values[0]), str(df_meta.loc[df_meta['station_no']==station, 'site_type_name'].values[0]), method=str(analysis_method))
        store['analysis_method'] = analysis_method
    
    # scatter plot met debietreeks
    dataQ = [go.Scatter(x=dff.index, y=dff['Value'], name=str(station), marker=dict(size=10), line=dict(color='#0a54cc'))]
    
    # scatter plot met overschrijdingskans debietreeks
    data_analysis = [go.Scatter(x=df_analysis['%'], y=df_analysis['Value'], name='Debietreeks overschrijding', mode='markers', marker=dict(size=10, color='#000000'))]

    # voeg berekende afvoeren als lijnen toe aan scatter plot met overschrijdingskans
    colormap = plt.cm.get_cmap('jet', len(discharges.index)-1)
    colors = [mpl.colors.rgb2hex(colormap(i)[:3]) for i in range(colormap.N)]
    color_count= 0
    for discharge in discharges.iterrows():
        if discharge[0] is not 'ID':
            data_analysis.append(go.Scatter(
                                    x=[0,100], y=[discharge[1][1],discharge[1][1]],
                                    name=str(discharge[0]), mode='lines', line=dict(color=str(colors[color_count]))))
            color_count += 1
    
    if tab == 'TDB':
        return html.Div(children=[
                            html.H4('GRAFIEKEN'), 
                            html.Div([
                                dcc.Graph(
                                      id='debiet', 
                                      figure={
                                        'data': dataQ,
                                        'layout': go.Layout(
                                                height=500,
                                                title='Debietmeting '+ str(df_meta.loc[df_meta['station_no']==station, 'name'].values[0]),
                                                yaxis=dict(title='Debiet [m^3/s]'),
                                                xaxis=dict(
                                                    rangeselector=dict(
                                                        buttons=list([
                                                             dict(label='1m', count=1, step='month', stepmode='todate'),
                                                             dict(label='6m', count=6, step='month', stepmode='todate'),
                                                             dict(label='1y', count=1, step='year', stepmode='todate'),
                                                             dict(label='3y', count=3, step='year', stepmode='todate'),
                                                             dict(step='all')
                                                        ])
                                                    ),
                                                    rangeslider=dict(
                                                        visible = False
                                                    ),
                                                    type='date'
                                                    ),
                                                showlegend=False
                                                )}
                                            )
                                    ], style={'width': '50%', 'display': 'inline-block'}),
                            html.Div([
                                dcc.Graph(
                                    id='afvoeranalyse', 
                                    figure={
                                        'data': data_analysis,
                                        'layout': go.Layout(
                                            height=500,
                                            title='Afvoeranalyse '+ str(df_meta.loc[df_meta['station_no']==station, 'name'].values[0]),
                                            yaxis=dict(title='Debiet [m^3/s]'),
                                            xaxis=dict(type='linear'),
                                            showlegend=True
                                            )}
                                        )
                                    ], style={'width': '50%', 'display': 'inline-block'}),
                                html.Div([
                                        html.H4('TABEL [m^3/s]'), 
                                        html.Div(
                                           dash_table.DataTable(
                                                id='table',
                                                columns=[{'name': i, 'id': i} for i in discharges.transpose().columns[1:]],
                                                data=pd.DataFrame(discharges['discharge'].drop(columns='ID')).transpose().round(2).to_dict('rows'),
                                            )
                                       )], style={'width': '100%'}),
                                ])

    if tab == 'WTH':
        return html.Div(children=[
                    html.H4('GRAFIEKEN'), 
                    html.Div([
                        dcc.Graph(
                              id='waterhoogte', 
                              figure={
                                'data': dataQ,
                                'layout': go.Layout(
                                        height=500,
                                        title='Waterhoogtemeting '+ str(df_meta.loc[df_meta['station_no']==station, 'name'].values[0]),
                                        yaxis=dict(title='Waterhoogte [mNAP]'),
                                        xaxis=dict(
                                            rangeselector=dict(
                                                buttons=list([
                                                     dict(label='1m', count=1, step='month', stepmode='todate'),
                                                     dict(label='6m', count=6, step='month', stepmode='todate'),
                                                     dict(label='1y', count=1, step='year', stepmode='todate'),
                                                     dict(label='3y', count=3, step='year', stepmode='todate'),
                                                     dict(step='all')
                                                ])
                                            ),
                                            rangeslider=dict(
                                                visible = False
                                            ),
                                            type='date'
                                            ),
                                        showlegend=False
                                        )}
                                    )
                            ], style={'width': '50%', 'display': 'inline-block'}),
                    html.Div([
                        dcc.Graph(
                            id='waterhoogteanalyse', 
                            figure={
                                'data': data_analysis,
                                'layout': go.Layout(
                                    height=500,
                                    title='Waterhoogteanalyse '+ str(df_meta.loc[df_meta['station_no']==station, 'name'].values[0]),
                                    yaxis=dict(title='Waterhoogte [mNAP]'),
                                    xaxis=dict(type='linear'),
                                    showlegend=True
                                    )}
                                )
                            ], style={'width': '50%', 'display': 'inline-block'}),
                        html.Div([
                                html.H4('TABEL [mNAP]'), 
                                html.Div(
                                   dash_table.DataTable(
                                        id='table',
                                        columns=[{'name': i, 'id': i} for i in discharges.transpose().columns[1:]],
                                        data=pd.DataFrame(discharges['discharge'].drop(columns='ID')).transpose().round(2).to_dict('rows'),
                                    )
                               )], style={'width': '100%'}),
                        ])


#%% callback voor updaten dropdown waarde        
@app.callback(Output('station', 'value'),
             [Input('map', 'clickData')])

def update_dropdown(map_station):
    if map_station is None:
        return store['station']
    else:
        station_no = df_meta['station_no'].iloc[map_station['points'][0]['pointNumber']]    
        return station_no
    
#%% callback voor updaten rangeslider        
@app.callback(Output('period', 'marks'),
             [Input('graphs', 'children')])

def update_rangeslider_range(graphs):
    return {int(round(i)): '{}'.format(int(round(i))) for i in np.linspace(df.index.year.min(), df.index.year.max(), num=5, endpoint=True)}

#%% callback voor updaten rangeslider        
@app.callback(Output('period', 'min'),
             [Input('period', 'marks')])

def update_rangeslider_min(graphs):
    return df.index.year.min()

#%% callback voor updaten rangeslider        
@app.callback(Output('period', 'max'),
             [Input('period', 'min')])

def update_rangeslider_max(graphs):
    return df.index.year.max()
        
#%% callback voor updaten geselecteerd punt kaart  
@app.callback(Output('map', 'figure'),
             [Input('graphs', 'children')],
             [State('map', 'figure')])

def update_map_selected(graphs, existing_map_state):
    return {
        'data':[go.Scattermapbox(
                    lat=df_meta['station_latitude'], 
                    lon=df_meta['station_longitude'], 
                    mode='markers', 
                    marker=dict(size=10, color='#e88709'),
                    hoverinfo='text',
                    text=df_meta['name'],
                    ),
                go.Scattermapbox(
                    lat=[df_meta.loc[df_meta['station_no']==store['station'], 'station_latitude'].values[0]], 
                    lon=[df_meta.loc[df_meta['station_no']==store['station'], 'station_longitude'].values[0]],
                    mode='markers', 
                    marker=dict(size=14, color='#39e809'),
                    hoverinfo='text',
                    text=[df_meta.loc[df_meta['station_no']==store['station'], 'name'].values[0]],
                )], 
        'layout': go.Layout(
                height=400,
                autosize=True, 
                hovermode='closest', 
                mapbox=dict(
                    accesstoken=mapbox_access_token,
                    bearing=0,
                    center=dict(
                        lat=df_meta.loc[df_meta['station_no']==store['station'], 'station_latitude'].values[0],
                        lon=df_meta.loc[df_meta['station_no']==store['station'], 'station_longitude'].values[0]
                    ),
                    pitch=0,
                    zoom=10
                    ),
                margin=dict(r=0, t=0, b=0, l=0, pad=0),
                showlegend=False
                )
        }
#%% callback voor updaten tabblad
@app.callback(Output('station', 'options'),
             [Input('tabs', 'value')])

def update_tab(tab):
    
    global df_meta
    
    df_meta = func.getMetaData(tab)
    
    return [{'label': label, 'value': value} for value, label in zip(df_meta['station_no'], df_meta['name'])]

#%% start dashboard op localhost
if __name__ == '__main__':
    app.run_server(
            port=80
            )
