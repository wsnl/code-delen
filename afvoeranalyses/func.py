# -*- coding: utf-8 -*-
"""
Created on Wed Oct 17 16:00:27 2018

@author: Thomas
"""

import requests

import numpy as np
import pandas as pd

pd.options.mode.chained_assignment = None # turn off SettingWithCopyWarning

from lmoments3 import distr

def getMetaData(tab):
    """Returns GeoDataFrame with locations and metadata for given tab 
    
    Parameters
    ----------
    tab: str, ('TDB': Totaaldebiet, 'WTH': Waterhoogte)
        abbreviation used in WISKI for measurement parameter, 
    
    Returns
    -------
    gdf: GeoDataFrame
        GeoDataFrame containing geolocation and metadata for given tab.
    """

    url = "http://10.10.3.126:8080/KiWIS/KiWIS?datasource=0&service=kisters&type=queryServices&request=getStationList&format=json&returnfields=station_id,station_no,site_name,station_latitude,station_longitude,stationparameter_name"
    r = requests.get(url) # url met alle stations
    df = pd.DataFrame(r.json()[1:], columns=r.json()[0]) # json naar DataFrame
    df = df[df['station_no'].str.contains(tab)] # selecteer alle stations met tab-value, bijv. 'TDB', Debiet
    df = df[pd.to_numeric(df['station_latitude'], errors='coerce')>-99999]
    if tab == 'TDB':
        df = df[df['stationparameter_name']=='Debiet'] # selecteer nogmaals enkel debietstations
    else:
        df = df[df['stationparameter_name']=='Waterhoogte'] # voor waterhoogteanalyse
        df = df[~df['station_no'].str.contains('KST')] # neem de handmetingen niet mee
        
    df[['station_latitude', 'station_longitude']] = df[['station_latitude', 'station_longitude']].apply(pd.to_numeric, errors='coerce') # x-y coordinaten omzetten naar numeriek
    
    if tab == 'TDB':
        url_site_type_name = 'http://10.10.3.126:8080/KiWIS/KiWIS?service=kisters&type=queryServices&request=getSiteList&datasource=0&format=json&returnfields=site_no,site_type_name'
        r = requests.get(url_site_type_name)
        site_type_name =  pd.DataFrame(r.json()[1:], columns=r.json()[0]) # DataFrame met site_nos & site_type
        site_type_name['site_no'] = site_type_name['site_no']+'_TDB' # station_no is site_no + '_TDB'
    
        df = df.merge(site_type_name, how='left', left_on='station_no', right_on='site_no') # site_type toevoegen aan DataFrame
        df = df.dropna()
        df['name'] = df['site_name'].copy()
    else:
        df['name'] = df['site_name'].copy() + ' ' + df['station_no'].copy()
        df['site_type_name'] = 'waterhoogte'
        
    # converteer meta DataFrame naar GeoDataFrame voor kaartje
    from shapely.geometry import Point
    import geopandas as gpd
    
    gdf = gpd.GeoDataFrame(df, geometry=[Point(x,y) for x, y in zip(df.station_longitude, df.station_latitude)]) # geometry toevoegen

    return gdf

def getData(station, param):
    """Returns DataFrame with complete measurement series
    
    Parameters
    ----------
    station: str
        station_no used in WISKI
    param: str
        stationparameter_name used in WISKI
    
    Returns
    -------
    df: DataFrame
        DataFrame containing complete measurement series
    """

    url = "http://10.10.3.126:8080/KiWIS/KiWIS?service=kisters&type=queryServices&request=getTimeseriesList&datasource=0&format=json&stationparameter_name=" + str(param) + "&ts_name=Dag.Gem&station_no=" + str(station) + "&returnfields=ts_id"
    r = requests.get(url) # haal timeseries_id binnen voor combinatie van station & param
    str_ts_id = str(r.json()[1][0]) # maak een string van eerste ts_id
    
    url = "http://10.10.3.126:8080/KiWIS/KiWIS?service=kisters&type=queryServices&request=getTimeseriesValues&datasource=0&format=json&period=complete&ts_id=" + str_ts_id
    r = requests.get(url) # haal complete meetreeks binnen voor ts_id
    df = pd.DataFrame(r.json()[0]['data'], columns = ['datum', 'Value']) # laat reeks in
    df['datum'] = pd.to_datetime(df['datum']) # datetime conversie
    df = df.set_index('datum') # maak datetime index
    df = df.dropna()
    df = df.resample('D').mean() # resample naar dagwaarden

    return df

def dischargeAnalysis(df, name, site_type_name, method='cultuur'):
    """Returns a list containing the discharge analysis values and the modified DataFrame
    
    Parameters
    ----------
    df: DataFrame
        station_no used in WISKI
    name: str
        site_name used in WISKI
    site_type_name: str
        site_type_name used in WISKI, e.g. Oppervlaktewatergemaal, Stuw
    method: str ('cultuur', 'gumbel', 'seizoens')
        method used to calculate statistics
    
    Returns
    -------
    discharges: list
        list containing the calculated statistics for the discharges
    df: DataFrame
        the modified DataFrame, containing the rankingcurve
    """
    
    if method =='seizoens':
        discharges = pd.DataFrame() # maak lege DataFrame
        dfg = df.groupby(df.index.month).median() # groepeer de debieten bij maand en bereken de maandelijkse mediaan
        
        # bepaal de mediane afvoer per seizoen
        discharges.at['ID', 'station_no'] = name
        discharges.at['ID', 'discharge'] = df.max()[0] # maximaal debiet
        discharges.at['voorjaar', 'discharge'] = dfg.loc[3:5].mean()[0] # maart tot en met mei
        discharges.at['zomer', 'discharge'] = dfg.loc[6:8].mean()[0] # juni tot en met augustus
        discharges.at['herfst', 'discharge'] = dfg.loc[9:11].mean()[0] # september tot en met november
        discharges.at['winter', 'discharge'] = dfg.iloc[[0,1,11],:].mean()[0] # december tot en met februari
    
        # duurkromme
        count = df['Value'].count() # aantal metingen
        years = count / 365 # aantal jaren metingen
        df['rank'] = df['Value'].rank(method = 'first') # sorteer de metingen van laag naar hoog
        df['%'] = df['rank'] / count * 100 # bereken percentage overschrijding
        
    else:
        # vaak voorkomende afvoeren bepalen
        discharges = pd.DataFrame(data = np.array([[0,0], [358, 0], [335, 0], [200, 0], [100, 0], [15, 0], [1, 0], [1, 0], [1, 0], [1, 0], [1, 0]]),
                                  index = ['ID', 'droogste week', 'droogste maand', 'vaak voorkomend', 'voorjaar', 'halfmaatgevend', 'T1 maatgevend', 'T10 afvoer', 'T25 afvoer', 'T50 afvoer', 'T100 afvoer'],
                                  columns = ['#days', 'discharge'])
           
        # duurkromme
        count = df['Value'].count() # aantal metingen
        years = count / 365 # aantal jaren metingen
        df['rank'] = df['Value'].rank(method = 'first') # sorteer de metingen van laag naar hoog
        df['%'] = df['rank'] / count * 100 # bereken percentage overschrijding
        
        # bepaal de vaak voorkomende afvoeren
        # zoek de afvoer in gesorteerde dagafvoeren die hoort bij de overschrijding, dus overschrijding * jaren
        discharges['discharge'] = discharges['#days'].apply(lambda x: df.loc[df['rank'] == round(count - x * years), 'Value'].values[0])      

        # bepalen van extremen voor debieten van alle niet gemalen        
        if not ((site_type_name.find("gemaal")>-1) or (site_type_name.find("Gemaal")>-1) or (site_type_name.find("waterhoogte")>-1)):
            T_discharge = np.array([10,25,50,100]) # herhalingstijden voor de te bepalen extremen
            
            # methode om extreme afvoer te bepalen obv schaling uit cultuurtechnisch vademecum
            if str(method) == 'cultuur':
                factors = np.array([1.44, 1.6, 1.8, 2])  # factoren uit Cultuurtechnisch Vademecum om maatgevende afvoer te schalen naar extreme afvoeren
                discharges.iloc[7:,1] = discharges.loc['T1 maatgevend', 'discharge'] * factors # bereken de extremen als maatgevende afvoer maal factor
            # methode om extreme afvoer te bepalen met gumbel  
            elif str(method) == 'gumbel':
                maxq = df.Value.resample('a').max().squeeze() # maxima per jaar in serie
                theta = distr.gev.lmom_fit(maxq) # bepaal verdelingsparameters obv jaar maxima
                gev = distr.gev(**theta) # initieer generalized extreme value verdeling met de hierboven bepaalde parameters
                discharges.iloc[7:,1] = gev.ppf(1.0-1./T_discharge) # bepaal de debieten behorende bij de herhalingstijden en schrijf weg
        else:
            discharges = discharges.iloc[:7,:].copy()

    return discharges.round(2), df


