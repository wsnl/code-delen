# code-delen

Delen van codes, ideeën en informatie tussen de waterschappen & RWS.

## Gebruik
Zie als voorbeeld de [afvoeranalyse](https://gitlab.com/wsnl/code-delen/tree/master/afvoeranalyses). Het zou mooi zijn als jullie ook een korte README.md schrijven met daarin de uitleg van wat het doet, misschien een plaatje of iets dergelijks en wat contactgegevens (.md betekent Markdown, google maar even en je komt er wel aan uit). 